# Thesis

This file describes how to set up the different repos in this project to use the code.


## Setup repo (on cloud VM)

```bash
mkdir thesis
cd thesis
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/overview.git

./overview/setup.sh
```

If you prefer to do each step manually, see below.

### Download repos

```bash
mkdir thesis
cd thesis
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/code.git
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/data.git
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/web.git

sudo apt install python3-pip
pip3 install --upgrade pip

# install for GPU & torch differs so update requirements list 
# and view http://pytorch.org/ for exact details.
cd code
pip3 install -r requirements.txt
```

### Format data

```bash
ipython
> import nltk
> nltk.download('punkt')
./scripts/process.sh
```

## Start up jupyter notebook

To explore and interact with the code, start up a jupyter notebook. 
Also, when setting up your VM add port forwarding to 8888 for jupyter.

```bash
screen
jupyter notebook --no-browser
# Note the access token

# exit with  
`ctrl-a d`

# connect with:
screen -r
```

## Connect to jupyter notebook

```bash
ssh -L 8889:localhost:8888 {user}@{server_address}
```

Navigate to: http://localhost:8889
