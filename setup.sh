#--- setup.sh ---#

# note

echo "This is not tested. When in doubt follow the instructions in `README.md`"

# clone additional repos
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/code.git
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/data.git
git clone https://cjlovering@bitbucket.org/cjlovering-ms-thesis/web.git

# install and update pip3
sudo apt install python3-pip
pip3 install --upgrade pip

# download dependencies
cd code
pip3 install -r requirements.txt

# process data and create outputed data
./scripts/process.sh
./scripts/test.sh
cd ..
